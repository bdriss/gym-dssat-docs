# simple top-level makefile

URI = ./docs/build/html/index.html

build-docs:
	sphinx-build -b html docs/source/ docs/build/html

clean:
	rm -rf docs/build/

render:
	sensible-browser $(URI) || xdg-open $(URI) || open $(URI)

build: clean build-docs