# Source of the **gym-dssat** documentation

This is repository contains source of the documentation of **gym-dssat** Reinforcement Learning environment. The documentation uses [Sphinx](https://www.sphinx-doc.org/) with the [custom book theme](https://github.com/executablebooks/sphinx-book-theme).

After each ```git push```, the documentation is automatically rebuilt and updated on [https://rgautron.gitlabpages.inria.fr/gym-dssat-docs/](https://rgautron.gitlabpages.inria.fr/gym-dssat-docs/).