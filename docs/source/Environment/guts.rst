.. _guts:

Under the hood
==============

**gym-dssat** uses a modification of DSSAT, called **dssat-pdi** (see `here <https://gitlab.inria.fr/rgautron/dssat-csm-os>`_ in Gitlab). **dssat-pdi** is powered by the `PDI Data Interface (PDI) <https://gitlab.maisondelasimulation.fr/pdidev/pdi>`_. PDI allows to turn the monolithic Fortran program into a Reinforcement Learning environment by pausing its execution, retrieving DSSAT's internal state, interacting with Python, specifiying actions and finally resuming its execution as shows the Figure :ref:`pdi`. The Figure :ref:`data streams` presents the different parts behind the **gym-dssat** environment. **gym-dssat**, which is from a user perspective the usual `Open AI gym interface <https://gym.openai.com/>`_, communicates with **dssat-pdi** through `PyZMQ sockets <https://github.com/zeromq/pyzmq>`_.

The modification of DSSAT using PDI require minimal additions of code snippets. DSSAT being in continuous development, we are able to easily convey DSSAT updates to **dssat-pdi**.

.. _pdi:

.. figure:: /_static/figures/pdi.png
   :align: center
   :width: 70%

   **dssat-pdi** : a modification of DSSAT using PDI

.. _data streams:

.. figure:: /_static/figures/dataStreams.png
   :align: center
   :width: 80%

   Components behind **gym-DSSAT** 