Rewards
=======

Each default decision problem has its own reward function designed to make challenging problems taking into account the costs of actions and the environmental factors. This is achieved following the reward shaping principle [1]_, [2]_. Rewards are scalar, excepted for the both nitrogen fertilization and irrigation problem: the reward is a vector of the two respective individual reward functions.

For instance, for the fertilization problem nitrogen applications are penalized for any non null quantity ; nitrate leaching and volatilization are penalized as well ; plant nitrogen intake is rewarded.

.. Hint:: For more details, reward functions are explicitely defined in a `separated Python file <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/rewards.py>`_.

References
^^^^^^^^^^

.. [1] Randløv, J. and Alstrøm, P. (1998). Learning to drive a bicycle using reinforcement learning and shaping. In ICML, volume 98, pages 463–471. Citeseer.

.. [2] GNg, A. Y., Harada, D., and Russell, S. (1999). Policy invariance under reward transformations: Theory and application to reward shaping. In Icml, volume 99, pages 278–287.