.. _observation space:

Observation spaces
==================

Observation space definitions
-----------------------------


Our goal with **gym-dssat** is to provide real-world like crop management problems. As a consequence, the criteria of choice to define the observations to provide to the agent are the variables to be limited in number and realistically measurables/proxied in a real crop field. The internal crop simulator --named DSSAT-- has literaly hundreds of internal state variables. Based on expert knowledge, we chose a subset of DSSAT's internal variables to be available in **gym-dssat**, and this subset is further restricted for the observations provided to the agent during the interactions. The Figure :ref:`state space` show the succesive subsets of DSSAT's original state features. Dashed arrows show the files defining the restrictions.

.. Note:: For more information about how **gym-dssat** works, you can check out the Section :ref:`guts`.

.. _state space:

.. figure:: /_static/figures/stateSpace.png
   :align: center
   :width: 70%

   Successive restrictions on DSSAT state features


The observation space --i.e. from agent's perspective-- depend on each mode and are detailed in `gym environment's yaml configuration file <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/env_config.yml>`_. State variables can be continuous, discrete and arrays of arbitrary shapes. Actions are continuous. For instance, in the ``setting`` key of the YAML file, you can find:

.. code-block:: yaml

   #######################################################
   # ACTION AND STATE SPACES RESTRICTIONS FOR ENV SETTINGS <- where settings are defined
   #######################################################
    setting:
      fertilization:
        action: # action space
          - anfer
        state: # observation space
          - istage
          - vstage
          - grnwt
          - topwt
          - swfac
          - nstres
          - xlai
          - dtt
          - dap
          - cumsumfert
          - ep
        context:  # extra information the gym `info'
          - sat
          - dul
          - ll
          - dlayr
        experiment_number: 1

.. Hint:: You can access the current values of the observation variables provided to the agent with the attribute ``env.observation``.

Each individual variable definition, including agronomic meaning, can be found in the ``state`` key of the raw state space of the aforementioned YAML file, as exemplified below:

.. code-block:: yaml
   
    #################
    # RAW STATE SPACE <- where all state variables imported from DSSAT-PDI are defined
    #################
    state:
      istage:
         type: discrete
         info: DSSAT maize growing stage (index in order 7, 8, 9, 1, 2, 3, 4, 5 ,6)
         size: 9
         # 1 = End of Juvenile Phase
         # 2 = Floral Initiation
         # 3 = Silking
         # 4 = Beginning of Grain Filling
         # 5 = End of Grain Filling
         # 6 = Maturity
         # 7 = Sowing
         # 8 = Germination
         # 9 = Emergence
      vstage:
        type: float
        low: 0
        high: 30
        info: vegetative growth stage (number of leaves)
      ...

.. Hint:: You can access all the values of the full gym-DSSAT observation space (including variables hidden to the agent) with the attribute ``env._state``. More information is available in the :ref:`attributes` Section.

Getting information
-------------------

After your environment instance is initialized, you can get information about the action and observation spaces of the current mode using:

.. code-block:: python
    
    env.get_env_info()

You will get an interactive output, as exemplified below:


.. code-block:: shell

    ******************
    Available actions:
    ******************

    {'anfer': {'high': 200,
            'info': 'nitrogen to fertilize for current day (kg/ha)',
            'low': 0,
            'type': 'float'}}
    press "return" to continue
    {'amir': {'high': 50,
            'info': 'water depth to irrigate for current day (mm/m2)',
            'low': 0,
            'type': 'float'}}
    press "return" to continue

    *********************
    Observation variables:
    *********************

    {'cleach': {'high': inf,
                'info': 'cumulative nitrate leaching (kg/ha)',
                'low': 0,
                'type': 'float'}}
    press "return" to continue
    ...
