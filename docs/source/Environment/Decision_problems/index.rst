.. _decision problems:

Decision problems
=================

The environment comes with 3 default modes defining 3 decision problems:

* | **Nitrogen fertilization only** (continuous quantity): ``mode=='fertilization'``
  |   → an action is a nitrogen fertilizer quantity (kg/ha)

* | **Irrigation only** (continuous quantity): ``mode=='irrigation'``
  |   → an action is a water quantity (L/m2)

* | **Both nitrogen fertilization and irrigation** (both continuous quantities): ``mode=='all'``
  |   → an action is both fertilizating and irrigating

The environment is defined at field scale. One time step corresponds to one day. An episode runs from prior to planting to crop harvest which is automatically done at crop maturity. A complete episode, also referred as a growing season, lasts about 160 days. In case of crop failure, the episode stops earlier. During each episode, the weather is stochastically generated. The decision problems in DSSAT are Partially Observable Markov Decision Processes (POMDP) [1]_.

.. toctree::
   :maxdepth: 1

   observation_space
   rewards

.. Note:: If you want to learn more about the simulations behind, please checkout the Section :ref:`simulations`. 

.. Note:: If you want to define your custom decision problems, you can checkout the Advanced Usage's Section :ref:`custom problems`.

References
^^^^^^^^^^

.. [1] Karl Johan Åström. Optimal control of markov processes with incomplete state information i. Journal of Mathematical Analysis and Applications, 10:174–205, 1965.