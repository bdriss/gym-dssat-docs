.. _simulations:

Simulations
===============

DSSAT
-----

gym-dssat simulations are computed with a modification of the Decision Support System for Agrotechnology Transfer (`DSSAT <https://dssat.net/>`_) Fortran software is a crop simulator that is widely recognized by agricultural decision makers, from farmers to researchers. DSSAT has been developped for the last 30 years and is continuously integrating new knowledge. It is used in more than 170 countries around the world. DSSAT internally work on a daily basis, with the integration partial differential equations describing biological, chemical and physical processes at stake [1]_.

Field experiment
--------------------

**gym-dssat** default problems are based on the UFGA8201 original maize experiment [2]_. This experiment has been carried out in 1982 at the University of Florida (Gainesville, FL, USA) and has been used as a reference to develop DSSAT.

Weather generator
---------------------

**gym-dssat** default problems use the WGEN [3]_ built-in stochastic weather generator. With then argument ``random_weather=True``, a new weather time series is generated for each episode in **gym-dssat**. WGEN is based on a first-order Markov chain ; the rest of DSSAT's execution is deterministic. The stochastic weather generator has been parametrized based on more than 50 years of historical weather data from the University of Florida's experimental farm.

References
^^^^^^^^^^

.. [1] Hoogenboom, G., C.H. Porter, K.J. Boote, V. Shelia, P.W. Wilkens, U. Singh, J.W. White, S. Asseng, J.I. Lizaso, L.P. Moreno, W. Pavan, R. Ogoshi, L.A. Hunt, G.Y. Tsuji, and J.W. Jones. 2019. The DSSAT crop modeling ecosystem. In: p.173-216 [K.J. Boote, editor] Advances in Crop Modeling for a Sustainable Agriculture. Burleigh Dodds Science Publishing, Cambridge, United Kingdom `(http://dx.doi.org/10.19103/AS.2019.0061.10) <http://dx.doi.org/10.19103/AS.2019.0061.10>`_

.. [2] LA Hunt and KJ Boote. Data for model operation, calibration, and evaluation. In Understanding options for agricultural production, pages 9–39. Springer, 1998.

.. [3] CW Richardson. Weather simulation for crop management models. Transactions of the ASAE, 28(5):1602–1606, 1985.