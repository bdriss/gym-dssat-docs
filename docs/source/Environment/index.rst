.. _environment:

The Environment
===============

By default **gym-DSSAT** uses the UFGA8201 maize experiment from the University of Florida, but is usable with any DSSAT experiment using the CERES-Maize module.

.. toctree::
   :maxdepth: 2

   Decision_problems/index
   dssat
   guts