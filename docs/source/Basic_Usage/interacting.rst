Interacting with gym-DSSAT
==========================

**gym-DSSAT** works in a daily basis: 1 time step = 1 day, and so interactions are daily based!

Observations
------------

The ``observation`` variable is a dictionnary. The current observation can be retrieved via  ``env.observation``:

.. code-block:: python

    {'cleach': 39.01179885864258,
    'cnox': 0.07707925885915756,
    'cumsumfert': 116.0,
    'dap': 126,
    'dtt': 20.900001525878906
    ...}

.. Hint:: This dictionary can be turned into a list using the ``gym_dssat_pdi.envs.utils.utils.transpose_dicts`` helper.

Making action(s)
----------------

Actions are provided to the environment in a dictionary:

.. code-block:: python

    action_dict = {
        'anfer': 5,  # if mode == fertilization or mode == all ; nitrogen to fertilize in kg/ha
        'amir': 10,  # if mode == irrigation or mode == all ; water to irrigate in L/ha
    }
    observation, reward, done, info = env.step(action_dict=action_dict)  # info are contextual variables

An example of episodic interactions
-----------------------------------

This dictionary can be concatenated to a list using  ``env.observation_dict_to_array(env.observation)``. When crop is harvested,  ``env.done`` is flagged ``True``.
An example of episode (for ``mode=='all'``) is given by:

.. code-block:: python

    while not env.done:
        observation = env.observation
        print(observation)
        # observation_list = env.observation_dict_to_array(observation)
        action_dict = {'anfer': 5 , 'amir': 10} # put 5 kg/ha of nitrogen and irrigate 10 L/m2
        observation, reward, done, info = env.step(action_dict)

The info variable contains contextual informations. At any time, you can access the whole history for the ongoing episode with ``env.history``.

.. Caution:: You must provide in ``action_dict`` (at least) all the actions required for the current mode. Example: if ``mode==all`` which means both fertilization and irrigation, you must provide values for both ``anfer`` and ``amir`` keys in the dictionary. If during interactions you do not want to fertilize or irrigate, simple set the value to 0.

Reseting after an episode ends
------------------------------

Once crop is harvested, which is equivalent to ``env.done == True``, you need to reset the environment with ``env.reset()``. Else, you will not get any new state!

Graceful termination
--------------------

Once you're done, terminate gym-DSSAT:

.. code-block:: python

    env.close()