Seeding and reproducibility
===========================

The result of **gym-dssat** are reproducible on the same platform, i.e. same software and hardware layers. You can specify the seed with ``seed`` argument at environment's :ref:`initialization`. The seed fixes as well the internal stochastic weather generation.

.. Caution:: We cannot guarantee cross-platform reproducibility due to the multiple factors at stake.

If you want to fix or change the seed of an already initialized **gym-dssat** instance named ``env``, you can use:

.. code-block:: python

    seed = 123
    env.set_seed(seed)