.. _package installation:

With packages (recommended)
===========================

Available Linux distributions are:

* `Debian11 (Bullseye) <https://www.debian.org/releases/bullseye/>`_: referred as ``bullseye``

* `Debian Unstable (Sid) <https://www.debian.org/releases/sid>`_: referred as ``sid``

* `Ubuntu 22.10 (Kinetic Kudu) <http://releases.ubuntu.com/kinetic>`_: referred as ``kinetic``

* `Ubuntu 22.04 (Jammy Jellyfish) <http://releases.ubuntu.com/jammy>`_: referred as ``jammy``

* `Ubuntu 20.04 LTS (Focal Fossa) <http://releases.ubuntu.com/focal>`_: referred as ``focal``

.. Note:: Using the pre-compiled binary package installation,  **gym-DSSAT** comes with its proper Python virtual environment located in ``/opt/gym_dssat_pdi/bin/activate``.

Package installation
--------------------

Debian 11 (Bullseye)
^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

   echo "deb [ arch=amd64 ] https://raw.githubusercontent.com/pdidev/repo/debian bullseye main" | sudo tee /etc/apt/sources.list.d/pdi.list > /dev/null
   sudo wget -O /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg https://raw.githubusercontent.com/pdidev/repo/debian/pdidev-archive-keyring.gpg
   sudo chmod a+r /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg /etc/apt/sources.list.d/pdi.list
   sudo apt update
   sudo apt install pdidev-archive-keyring

.. code-block:: shell

   sudo mkdir -p /usr/local/share/keyrings
   sudo wget -O- http://gac.udc.es/gym-dssat/gym-dssat-archive-keyring.gpg | sudo tee /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg > /dev/null
   sudo echo "deb [signed-by=/usr/local/share/keyrings/gym-dssat-archive-keyring.gpg] http://gac.udc.es/gym-dssat bullseye main dev" | sudo tee /etc/apt/sources.list.d/gym-dssat.list > /dev/null
   sudo chmod a+r /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg
   sudo apt update
   sudo apt install gym-dssat-pdi

Debian Unstable (Sid)
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

   echo "deb [ arch=amd64 ] https://raw.githubusercontent.com/pdidev/repo/debian sid main" | sudo tee /etc/apt/sources.list.d/pdi.list > /dev/null
   sudo wget -O /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg https://raw.githubusercontent.com/pdidev/repo/debian/pdidev-archive-keyring.gpg
   sudo chmod a+r /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg /etc/apt/sources.list.d/pdi.list
   sudo apt update
   sudo apt install pdidev-archive-keyring

.. code-block:: shell

   sudo mkdir -p /usr/local/share/keyrings
   sudo wget -O- http://gac.udc.es/gym-dssat/gym-dssat-archive-keyring.gpg | sudo tee /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg > /dev/null
   echo "deb [signed-by=/usr/local/share/keyrings/gym-dssat-archive-keyring.gpg] http://gac.udc.es/gym-dssat sid main" | sudo tee /etc/apt/sources.list.d/gym-dssat.list > /dev/null
   sudo chmod a+r /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg
   sudo apt update
   sudo apt install gym-dssat-pdi

Ubuntu 20.04 (Focal Fossa)
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

   echo "deb [ arch=amd64 ] https://raw.githubusercontent.com/pdidev/repo/ubuntu focal main" | sudo tee /etc/apt/sources.list.d/pdi.list > /dev/null
   sudo wget -O /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg https://raw.githubusercontent.com/pdidev/repo/ubuntu/pdidev-archive-keyring.gpg
   sudo chmod a+r /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg /etc/apt/sources.list.d/pdi.list
   sudo apt update
   sudo apt install pdidev-archive-keyring

.. code-block:: shell

   sudo mkdir -p /usr/local/share/keyrings
   sudo wget -O- http://gac.udc.es/gym-dssat/gym-dssat-archive-keyring.gpg | sudo tee /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg > /dev/null
   echo "deb [signed-by=/usr/local/share/keyrings/gym-dssat-archive-keyring.gpg] http://gac.udc.es/gym-dssat focal main" | sudo tee /etc/apt/sources.list.d/gym-dssat.list > /dev/null
   sudo chmod a+r /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg
   sudo apt update
   sudo apt install gym-dssat-pdi

Ubuntu 22.04 (Jammy Jellyfish)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

   echo "deb [ arch=amd64 ] https://raw.githubusercontent.com/pdidev/repo/ubuntu jammy main" | sudo tee /etc/apt/sources.list.d/pdi.list > /dev/null
   sudo wget -O /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg https://raw.githubusercontent.com/pdidev/repo/ubuntu/pdidev-archive-keyring.gpg
   sudo chmod a+r /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg /etc/apt/sources.list.d/pdi.list
   sudo apt update
   sudo apt install pdidev-archive-keyring

.. code-block:: shell

   sudo mkdir -p /usr/local/share/keyrings
   sudo wget -O- http://gac.udc.es/gym-dssat/gym-dssat-archive-keyring.gpg | sudo tee /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg > /dev/null
   echo "deb [signed-by=/usr/local/share/keyrings/gym-dssat-archive-keyring.gpg] http://gac.udc.es/gym-dssat jammy main" | sudo tee /etc/apt/sources.list.d/gym-dssat.list > /dev/null
   sudo chmod a+r /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg
   sudo apt update
   sudo apt install gym-dssat-pdi

Ubuntu 22.10 (Kinetic Kudu)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

   echo "deb [ arch=amd64 ] https://raw.githubusercontent.com/pdidev/repo/ubuntu kinetic main" | sudo tee /etc/apt/sources.list.d/pdi.list > /dev/null
   sudo wget -O /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg https://raw.githubusercontent.com/pdidev/repo/ubuntu/pdidev-archive-keyring.gpg
   sudo chmod a+r /etc/apt/trusted.gpg.d/pdidev-archive-keyring.gpg /etc/apt/sources.list.d/pdi.list
   sudo apt update
   sudo apt install pdidev-archive-keyring

.. code-block:: shell

   sudo mkdir -p /usr/local/share/keyrings
   sudo wget -O- http://gac.udc.es/gym-dssat/gym-dssat-archive-keyring.gpg | sudo tee /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg > /dev/null
   echo "deb [signed-by=/usr/local/share/keyrings/gym-dssat-archive-keyring.gpg] http://gac.udc.es/gym-dssat kinetic main" | sudo tee /etc/apt/sources.list.d/gym-dssat.list > /dev/null
   sudo chmod a+r /usr/local/share/keyrings/gym-dssat-archive-keyring.gpg
   sudo apt update
   sudo apt install gym-dssat-pdi

Test your installation
----------------------
First, activate gym-dssat's Python virtual environment.

.. code-block:: shell

   source /opt/gym_dssat_pdi/bin/activate

then

.. code-block:: shell

   python /opt/gym_dssat_pdi/samples/run_env.py

After using gym-dssat, you can deactivate its Python virtual environment with:

.. code-block:: shell

   deactivate


Uninstalling packages
---------------------
To remove **gym-DSSAT** packages from your system you just need to run:

   sudo apt remove --purge gym-dssat-pdi dssat-pdi dssat-csm-data

After uninstalling gym-dssat, you probably want to get rid of other
packages that were automatically installed to satisfy gym-dssat
dependencies and maybe you do not need now. You can do it with:

   sudo apt autoremove
