.. _docker images:

Using Docker
============

Available Linux distributions for Dockerfiles are:

* `Debian11 (Bullseye) <https://www.debian.org/releases/bullseye>`_: referred as ``Dockerfile_Debian_Bullseye``

* `Debian Unstable (Sid) <https://www.debian.org/releases/sid>`_: referred as ``Dockerfile_Debian_Sid``

* `Ubuntu 22.10 (Kinetic Kudu) <http://releases.ubuntu.com/kinetic>`_: referred as ``Dockerfile_Ubuntu_Kinetic``

* `Ubuntu 22.04 LTS (Jammy Jellyfish) <http://releases.ubuntu.com/jammy>`_: referred as ``Dockerfile_Ubuntu_Jammy``

* `Ubuntu 20.04 LTS (Focal Fossa) <http://releases.ubuntu.com/focal>`_: referred as ``Dockerfile_Ubuntu_Focal``

Build an image
--------------
We take the example of the Dockerfile_Debian_Bullseye Dockerfile. To build a different Dockerfile, you just need to substitute the Dockerfile name as referred above.

To build an image called ``gym-dssat:debian-bullseye`` form the Dockerfile named ``Dockerfile_Debian_Bullseye``, simply run:

.. code-block:: shell

    docker build https://gitlab.inria.fr/rgautron/gym_dssat_pdi.git\#dev:docker_recipes -t "gym-dssat:debian-bullseye" -f Dockerfile_Debian_Bullseye 

Test a container
----------------

To check the ``gym-dssat:debian-bullseye`` image previously built, you can run the default example just with:

.. code-block:: shell

    docker run gym-dssat:debian-bullseye

Or you can interactively run the Docker image with:

.. code-block:: shell

    docker run -it gym-dssat:debian-bullseye bash


Docker images with Spack installation
=====================================

Additionaly, these Dockerfiles to build docker images using Spack are
provided. This recipes do not use pre-compiled packages, so gym-DSSAT
and all its dependencies are built from sources (see info about
gym-DSSAT Spack packages in :ref:`spack installation`):

* `Debian11 (Bullseye) <https://www.debian.org/releases/bullseye>`_: referred as ```Dockerfile_Debian_Bullseye_Spack```

* `Ubuntu 22.04 LTS (Jammy Jellyfish) <http://releases.ubuntu.com/jammy>`_: referred as ```Dockerfile_Ubuntu_Jammy_Spack```

* `Ubuntu 20.04 LTS (Focal Fossa) <http://releases.ubuntu.com/focal>`_: referred as ```Dockerfile_Ubuntu_Focal_Spack```

* `CUDA on Ubuntu 20.04 LTS (Focal Fossa) <http://releases.ubuntu.com/focal>`_: referred as ```Dockerfile_CUDA_Spack```

* `CentOS (latest) <https://www.centos.org>`_: referred as ```Dockerfile_CentOS_Spack```

These images can be built (it takes a long time, as every dependency
will be compiled) and tested the same way than the other gym-DSSAT
docker images.
