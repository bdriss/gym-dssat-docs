From source
===========

.. Caution:: if you install **gym-DSSAT** from source, you must use ``pdirun`` when launching your scripts. For instance: ``pdirun python3 run_env.py``. Ensure that ``pdirun`` path is correctly exported.

Dependencies
------------

* `CMake <https://cmake.org/install/>`_ >= 3.10

* C, C++ and `Fortran <https://fortran-lang.org/>`_ compilers, for instance GNU's gcc, g++ and `gfortran <https://gcc.gnu.org/wiki/GFortran>`_

* `Python <https://www.python.org/>`_ >= 3.8

* The following Python packages:

  * matplotlib==3.3.4

  * numpy==1.24.1

  * Jinja2

  * pyzmq

  * gym==0.21.0, higher gym versions are known to cause problems

.. Note:: Please follow the installation procedure in the same order as below.

1. PDI Data Interface (PDI)
---------------------------

In order to install the `PDI software <https://pdi.julien-bigot.fr/master/>`_, you can check the `official instructions <https://pdi.julien-bigot.fr/master/Installation.html>`_ but be **careful to correctly set cmake flags as shown below**.

Recommended installation directories are ``/opt/pdi`` or ``${HOME}/.pdi`` ; if possible avoid ``/usr/local/``. In the following we assume PDI to be installed in ``/opt/pdi``. You can find more details, including PID's dependencies `here <https://pdi.julien-bigot.fr/master/Installation.html>`_.

.. code-block:: shell

    git clone https://gitlab.maisondelasimulation.fr/pdidev/pdi.git
    mkdir pdi/build && cd pdi/build
    cmake -DCMAKE_INSTALL_PREFIX='/opt/pdi' \
        -DDIST_PROFILE=User \
        -DCMAKE_VERBOSE_MAKEFILE=ON \
        -DBUILD_CFG_VALIDATOR=OFF \
        -DBUILD_DECL_HDF5_PLUGIN=OFF \
        -DBUILD_DECL_NETCDF_PLUGIN=OFF \
        -DBUILD_DECL_SION_PLUGIN=OFF \
        -DBUILD_FLOWVR_PLUGIN=OFF \
        -DBUILD_FORTRAN=ON \
        -DBUILD_FTI_PLUGIN=OFF \
        -DBUILD_HDF5_PARALLEL=OFF \
        -DBUILD_MPI_PLUGIN=OFF \
        -DBUILD_PYCALL_PLUGIN=ON \
        -DBUILD_PYTHON=ON \
        -DBUILD_SET_VALUE_PLUGIN=ON \
        -DBUILD_SERIALIZE_PLUGIN=ON \
        -DBUILD_SHARED_LIBS=ON \
        -DBUILD_TEST_PLUGIN=OFF \
        -DBUILD_TRACE_PLUGIN=ON \
        -DBUILD_USER_CODE_PLUGIN=ON \
        -DUSE_DEFAULT=EMBEDDED .. # configuration
    sudo make install   # compilation and installation

2. DSSAT-PDI
------------

First, clone this repository and its submodules.

.. code-block:: shell

    git clone --recurse-submodules https://gitlab.inria.fr/rgautron/gym_dssat_pdi.git

From the root of this repository, supposing PDI has been installed in `/opt/pdi` and (modified) DSSAT to be installed in `/opt/dssat_pdi`:

.. code-block:: shell

    cd dssat-csm-os
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX='/opt/dssat_pdi' -DCMAKE_PREFIX_PATH='/opt/pdi/share/paraconf/cmake;/opt/pdi/share/pdi/cmake' ..
    make
    sudo make install

*Note: if for some reason DSSAT compilation failed, please empty the folder* ``dssat-csm-os/build`` *before retrying to compile DSSAT*

After then, you will need to provide DSSAT the required experimental files. From the root of this repository, assuming DSSAT has been installed in ``/opt/dssat_pdi`` :

.. code-block:: shell

    cd dssat-csm-data
    sudo cp -r ./* /opt/dssat_pdi

Finally, export DSSAT-PDI path with:

.. code-block:: shell
    
    export PATH=/opt/dssat_pdi:$PATH

.. Caution:: If DSSAT-PDI path is not exported, gym-DSSAT won't be able to find the ``run_dssat`` executable.

3. gym-DSSAT
------------

From the root of this repository:

.. code-block:: shell

    cd gym-dssat-pdi
    pip install -e .
