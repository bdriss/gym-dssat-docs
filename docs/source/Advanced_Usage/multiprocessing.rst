Handling multiprocessing
========================

**gym-dssat** supports multiprocessing as follows. Suppose you want to execute this piece of code:

.. code-block:: python

    with multiprocessing.Pool() as pool:
        results = pool.imap_unordered(multiproc_function, args))

Case 1 (recommended)
^^^^^^^^^^^^^^^^^^^^

You instantiate the environment within the process. We consider:

.. code-block:: python

    args = [env_args for _ in range(10)] # env_args being a dictionary of environment's arguments

Then, the correct definition of the ``multiproc_function`` follows:

.. code-block:: python

    def multiproc_function(env_args): # arg = env_args
        foo = None
        try:
            env = gym.make('gym_dssat_pdi:GymDssatPdi-v0', **env_args)
            ...
            foo = env.bar
        finally:
            env.close()
        return foo

Case 2
^^^^^^

You copy an already instantiated environment for your multiprocessing:

.. code-block:: python

    env = gym.make('gym_dssat_pdi:GymDssatPdi-v0', **env_args)

You first need to close the env:

.. code-block:: python

    env.close() # Necessary, else will fail !
    args = [env for _ in range(10)]

Then, the correct definition of the ``multiproc_function`` follows:

.. code-block:: python

    def multiproc_function(env):
        foo = None
        env.reset_hard()  # Necessary, else will fail !
        try:
            ...
            foo = env.bar
        finally:
            env.close()
        return foo

Remark
^^^^^^

.. Caution:: You need to explictely provide the seeds in the arguments of ``multiproc_function`` if you want variability