Observation space
=================

As introduced in Section :ref:`decision problems`, gym-DSSAT problems are POMDPs. In Section :ref:`observation space`, we shortly showed the successive subsets of DSSAT's internal variables to define the observations provided to the agent. By modifying the list of variables constituting the observations, you can modulate "the degree of partial observability" of the POMDP.

| The observation space can be customized by simply editing the `gym environment's yaml configuration file <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/env_config.yml>`_. For instance after package installation, you can simply edit:
| ``${GYM_DSSAT_PDI_PATH}/envs/configs/env_config.yml``

If we take the example of a new ``foo`` fertilization problem, we can define it following:

.. code-block:: yaml

   #################
   # RAW STATE SPACE
   #################
   state: # <- list of all DSSAT's internal features made available in gym-DSSAT 
   # please keep it untouched !
      vstage:
         type: float
         low: 0
         high: .inf
         info: vegetative growth stage (number of leaves)
      cleach:
         type: float
         low: 0
         high: .inf
         info: cumulative nitrate leaching (kg/ha)
      dtt:
         type: float
         low: 0
         high: 100
         info: growing degree days for current day (C/d)
      dul:
         type: array
         subtype: float
         low: 0
         high: 1
         size: 9
         info: volumetric soil water content in soil layers at drained upper limit (cm3 [water] / cm3 [soil])
      xlai:
         type: float
         low: 0
         high: .inf
         info: plant population leaf area index (m2_leaf/m2_soil)
      ...

   #######################################################
   # ACTION AND STATE SPACES RESTRICTIONS FOR ENV SETTINGS
   #######################################################
   setting:  
      foo:  # <- the new setting
         action:
            - anfer
         state:
            - cleach
            - dtt
            - vstage
         context:  # additional contextual information ; corresponds to gym's "info".
            - dul
         experiment_number: 1  # number of the experiment in DSSAT's fileX.

.. WARNING:: You cannot import new state features from DSSAT-PDI by simply adding variables to the ``state`` YAML key of the raw state space without modifying more deeply DSSAT-pdi. For the straighforward way, you can only modify the ``state`` key inside the ``setting`` key, for the considered setting as shown above.

.. Note:: For understanding what DSSAT's fileX is, please check `DSSAT's user guide <https://dssat.net/wp-content/uploads/2011/10/DSSAT-vol4.pdf>`_.
