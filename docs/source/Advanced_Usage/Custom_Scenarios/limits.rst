Limits
======

What you (currently) cannot do straighforwardly, we indicate with ★ the difficulty of implementation:

* use another crop than maize ★★★

* include other crop management operations, such as soil tillage or planting ★

If you want to implement those features with our support, you are welcome to `contribute <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/issues/new?title=Issue%20on%20page%20%2Findex.html&body=Your%20issue%20content%20here>`_!