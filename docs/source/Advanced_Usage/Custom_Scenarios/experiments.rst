DSSAT experimental files
==========================

Why?
^^^^

If you want to use any custom **maize** experiment, for instance with a custom soil or weather generation. 

.. Note:: Remember that **gym-DSSAT** currently only supports DSSAT's CERES-Maize module.

How?
^^^^

 If you want to use any maize experiment distinct from the default one, you can use the 3 last arguments as provided below:

.. code-block:: python

    env_args = {
        'log_saving_path': './logs/dssat-pdi.log',
        'mode': 'fertilization',
        'seed': 123456,
        'random_weather': True,
        'fileX_template_path': './my_custom_fileX.MZX',  # where the jinja2 fileX template is located
        'experiment_number': 1,  # the number of the experiment in the fileX to be run ; if specified overrides the parameter in environment's configuration YAML
        'auxiliary_file_paths': ['./my_custom_climate_file.CLI']  # required if random_weather is set True
    }

.. Caution:: The custom fileX template must present the same expressions ```{{ ... }}``` as the ones found in the `original example <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/UFGA8201.jinja2>`_. You can check `jinja documentation <https://jinja.palletsprojects.com/en/3.0.x/templates/>`_. Be careful, any faulty space in the template will raise errors. If you want custom stochastic climate generation, you must provide to DSSAT the corresponding ".CLI" file with the extra argument ``auxiliary_files_paths``. 

.. Caution:: ``my_custom_climate_file.CLI`` must be named after the 4 first characters of the value of ```WSTA``` in the custom fileX. For instance, if in ``./my_custom_fileX.MZX``` the variable ``WSTA``` is set to ``GAGR9626``, then the corresponding climate file must be named ``GAGR.CLI``. See `DSSAT's user guide <https://dssat.net/wp-content/uploads/2011/10/DSSAT-vol4.pdf>`_.