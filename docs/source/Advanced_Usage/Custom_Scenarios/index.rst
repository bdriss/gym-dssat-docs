.. _custom problems:

Creating custom problems
========================

gym-DSSAT is designed to allow a great setting flexibility. The figure :ref:`config files` shows the different configuration files used by the environment. The dashed boxes show settings which are straightforward to modify: we detail how in this Section.

.. _config files:

.. figure:: /_static/figures/config.png
   :align: center
   :width: 60%

   The configuration files of gym-dssat


.. toctree::
   :maxdepth: 1

   observation_space
   rewards
   experiments
   limits

.. Hint:: To know what **dssat-pdi** is respecting to **gym-dssat**, you can check out the Section :ref:`guts`.

.. Hint:: | If you installed **gym-dssat** with the instructions found in Section :ref:`package installation`, the configuration files are located in:
          | ``${GYM_DSSAT_PDI_PATH}/envs/configs/``
